<?php include('dist/pertials/header.php');?>

<?php include('dist/pertials/navbar.php');?>

    <?php
        include('connect.php');
        $id = $_GET['id'];
        $sql = "SELECT * FROM `students` WHERE id = $id";
        $table = $conn->query($sql);

        $result = $table->fetch_assoc();
    ?>

<section>
    <div class="container">
        <div class="row">
            <div class="form-field">
                <form action="update.php" method="POST">
                    <table>
                        <caption>Fill up using correct information</caption>
                        <input type="text" name="id" class="hidden" value="<?php echo $result['id']?>">
                        <tr>
                            <td><label for="name">Name</label></td>
                            <td><input type="text" name="name" class="form-control" id="name" value="<?php echo $result['name']?>" required></td>
                        </tr>
                        <tr>
                            <td><label for="class">Class</label></td>
                            <td>
                                <select id="class" name="class" class="form-control" required>
                                    <option value="<?php echo $result['class']?>" selected><?php echo $result['class']?></option>
                                    <option value="five">Five</option>
                                    <option value="six">Six</option>
                                    <option value="seven">Seven</option>
                                    <option value="eight">Eight</option>
                                    <option value="nine">Nine</option>
                                    <option value="ten">Ten</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="">Gender</label></td>
                            <td>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" id="" value="Male" <?php if($result['gender']=='Male'){echo "checked";} ?> /> Male
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" id="" value="female" <?php if($result['gender']=='female'){echo "checked";} ?> /> female
                                </label>
                                    <label class="radio-inline">
                                <input type="radio" name="gender" id="" value="Others" <?php if($result['gender']=='Others'){echo "checked";} ?> /> Others
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="address">Present address</label></td>
                            <td><input type="text" name="address" class="form-control" id="address" value="<?php echo $result['address']?>" required></td>
                        </tr>
                        <tr>
                            <td><label for="mobile">Mobile Number</label></td>
                            <td><input type="tel" name="mobile" class="form-control" id="mobile" value="<?php echo $result['mobile']?>" required></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" name="submit" class="form-control" value="Update"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('dist/pertials/footer.php');?>