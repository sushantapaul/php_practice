<?php include('dist/pertials/header.php');?>

<?php include('dist/pertials/navbar.php');?>

<?php session_start(); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="form-field">
                <form action="insert.php" method="POST">
                    <table>
                        <caption>Fill up using correct information</caption>
                        <div class="alert-danger">
                            <p>
                                <?php if(isset($_SESSION["message"])) {
                                    echo $_SESSION["message"];
                                    session_unset();
                                }
                                ?>
                            </p>
                        </div>
                        <tr>
                            <td><label for="name">Name</label></td>
                            <td><input type="text" name="name" class="form-control" id="name" placeholder="Your name" ></td>
                        </tr>
                        <tr>
                            <td><label for="class">Class</label></td>
                            <td>
                                <select id="class" name="class" class="form-control">
                                    <option value="" disabled selected>Your class</option>
                                    <option value="five">Five</option>
                                    <option value="six">Six</option>
                                    <option value="seven">Seven</option>
                                    <option value="eight">Eight</option>
                                    <option value="nine">Nine</option>
                                    <option value="ten">Ten</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="">Gender</label></td>
                            <td>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" id="inlineRadio1" value="male"> Male
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="gender" id="inlineRadio2" value="female"> female
                                </label>
                                    <label class="radio-inline">
                                <input type="radio" name="gender" id="inlineRadio3" value="others"> others
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="address">Present address</label></td>
                            <td><input type="text" name="address" class="form-control" id="address" placeholder="Your address"></td>
                        </tr>
                        <tr>
                            <td><label for="mobile">Mobile Number</label></td>
                            <td><input type="tel" name="mobile" class="form-control" id="mobile" placeholder="mobile number"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" name="submit" class="form-control" value="submit"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('dist/pertials/footer.php');?>
