<?php include('dist/pertials/header.php');?>

<?php include('dist/pertials/navbar.php');?>

<?php
    session_start();
    include('connect.php');
    $sql = "SELECT * FROM students";
    $table = $conn->query($sql);

?>

<section>
    <div class="container">
        <div class="row">
            <div class="form-field">
                <table>
                    <caption>All Student list</caption>
                    <div class="alert-danger">
                            <p>
                                <?php if(isset($_SESSION["message"])) {
                                    echo $_SESSION["message"];
                                    session_unset();
                                }
                                ?>
                            </p>
                        </div>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Class</th>
                            <th>Gender</th>
                            <th>Address</th>
                            <th>Mobile</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while($row = $table->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo $row['id']?></td>
                                <td><?php echo $row['name']?></td>
                                <td><?php echo $row['class']?></td>
                                <td><?php echo $row['gender']?></td>
                                <td><?php echo $row['address']?></td>
                                <td><?php echo $row['mobile']?></td>
                                <td>
                                    <a class="btn btn-primary btn-sm" href="show.php?id=<?php echo $row['id']?>">Show</a>
                                    <a class="btn btn-success btn-sm" href="edit.php?id=<?php echo $row['id']?>">Edit</a>
                                    <a class="btn btn-danger btn-sm" href="delete.php?id=<?php echo $row['id']?>">Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>


<?php include('dist/pertials/footer.php');?>